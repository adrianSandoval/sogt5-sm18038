#include <stdio.h>
#include <unistd.h>

int main() {
    char frase[31];
    ssize_t bytes_leidos;

    write(STDOUT_FILENO, "Ingrese una frase (máximo 30 caracteres): ", 42); 
    bytes_leidos = read(STDIN_FILENO, frase, sizeof(frase) - 1);

    if (bytes_leidos == -1) {
        perror("Error al leer la entrada estándar");
        return 1;
    }

    frase[bytes_leidos] = '\0'; 

    // Verificar si la longitud de la frase excede los 30 caracteres
    if (bytes_leidos > 30) {
        write(STDOUT_FILENO, "La frase es demasiado larga. Debe tener un máximo de 30 caracteres.\n", 65);
        return 1;
    }

    // Imprimir la frase tres veces 
    for (int i = 0; i < 3; i++) {
        write(STDOUT_FILENO, frase, bytes_leidos);
        write(STDOUT_FILENO, "\n", 1); // Agregar una nueva línea después de cada repetición
    }

    return 0;
}
