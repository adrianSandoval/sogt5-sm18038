#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int main() {
    const char *archivo_origen = "origen.txt"; 
    const char *archivo_destino = "destino.txt"; 

    int fd_origen = open(archivo_origen, O_RDONLY);
    if (fd_origen == -1) {
        perror("Error al abrir el archivo de origen");
        return 1;
    }

    // Crear o abrir el archivo de destino (si no existe, se crea)
    int fd_destino = open(archivo_destino, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fd_destino == -1) {
        perror("Error al abrir o crear el archivo de destino");
        close(fd_origen);
        return 1;
    }

    char *buffer;
    buffer=malloc(4096); 

    ssize_t num, bytes_escritos;

    while ((num = read(fd_origen, buffer, 4096)) > 0) {
        bytes_escritos = write(fd_destino, buffer, num);
        if (bytes_escritos != num) {
            perror("Error al escribir en el archivo de destino");
            close(fd_origen);
            close(fd_destino);
            free(buffer); 
            return 1;
        }
    }

    if (num == -1) {
        perror("Error al leer el archivo de origen");
        close(fd_origen);
        close(fd_destino);
        free(buffer);
        return 1;
    }

    close(fd_origen);
    close(fd_destino);
    free(buffer);

    printf("Se copio exitosamente el contendio de '%s' a '%s'\nPuedes verificarlo\n", archivo_origen, archivo_destino);
    return 0;
}
