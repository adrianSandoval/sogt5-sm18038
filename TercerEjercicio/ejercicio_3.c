#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
    const char *nombre_archivo = "archivo.txt"; 

    int fd_archivo = open(nombre_archivo, O_RDONLY);

    if (fd_archivo == -1) {
        perror("Error al intentar abrir el archivo");
        return 1;
    }

    char *buffer;
    buffer = malloc(4096); 
    ssize_t num;

    while ((num = read(fd_archivo, buffer, 4096)) > 0) {
        ssize_t bytes_escritos = write(STDOUT_FILENO, buffer, num);
        if (bytes_escritos == -1) {
            perror("Error al escribir en la salida");
            close(fd_archivo);
            free(buffer);
            return 1;
        }
    }

    if (num == -1) {
        perror("Error al leer el archivo");
        close(fd_archivo);
        free(buffer); 
        return 1;
    }

    close(fd_archivo);
    free(buffer); 
    return 0;
}
