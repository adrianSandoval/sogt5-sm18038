#include <unistd.h>
#include <string.h>  

int main() {
    const char *mensaje = "Sistemas Operativos 2023\r\n";
    ssize_t bytes_escritos;

    bytes_escritos = write(1, mensaje, strlen(mensaje)); 

    if (bytes_escritos == -1) {
        return 1;
    }
    
    return 0;
}

